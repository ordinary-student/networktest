﻿using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace NetworkTest
{
    //表格辅助类
    public class DataGridViewHelper
    {
        //统计设备数量
        public static int CountDevices(DataGridView d)
        {
            int sum = 0;
            //遍历DataGridView
            for (int i = 0; i < d.Rows.Count; i++)
            {
                for (int j = 0; j < d.Rows[i].Cells.Count; j++)
                {
                    //是合法IP，计数+1
                    if (d[j, i].Value != null && IPHelper.IsIP(d[j, i].Value.ToString()))
                    {
                        sum++;
                    }
                }
            }

            //返回
            return sum;
        }

        //获取待检测IP数组
        public static string[] GetIPs(DataGridView d)
        {
            //待检测IP数组
            string[] ips = new string[CountDevices(d)];
            int k = 0;
            //遍历DataGridView
            for (int i = 0; i < d.Rows.Count; i++)
            {
                for (int j = 0; j < d.Rows[i].Cells.Count; j++)
                {
                    if (d[j, i].Value != null)
                    {
                        //IP-列坐标，行坐标
                        string ip = d[j, i].Value.ToString();
                        //是合法IP
                        if (IPHelper.IsIP(ip))
                        {
                            ips[k] = ip;
                            k++;
                        }
                    }
                }
            }
            //
            return ips;
        }

        //初始化所有单元格背景
        public static void InitCellsBackground(DataGridView d, Color color)
        {
            //遍历DataGridView
            for (int i = 0; i < d.Rows.Count; i++)
            {
                for (int j = 0; j < d.Rows[i].Cells.Count; j++)
                {
                    d[j, i].Style.BackColor = color;
                }
            }
        }

        //根据Ping结果设置单元格背景颜色
        public static void SetCellBackColor(DataGridView d, PingResult r2)
        {
            //遍历DataGridView
            for (int i = 0; i < d.Rows.Count; i++)
            {
                for (int j = 0; j < d.Rows[i].Cells.Count; j++)
                {
                    //判断内容=IP 
                    if (d[j, i].Value != null && d[j, i].Value.ToString() == r2.IP)
                    {
                        //判断结果
                        if (r2.State)
                        {
                            //true
                            d[j, i].Style.BackColor = Tool.GetColorByDelay(r2.RoundtripTime);
                        }
                        else
                        {
                            //false，粉红色
                            d[j, i].Style.BackColor = Color.Pink;
                        }
                    }
                }
            }
        }

        //根据内容查找单元格
        public static DataGridViewCell FindCell(DataGridView d, string content)
        {
            //遍历DataGridView
            for (int i = 0; i < d.Rows.Count; i++)
            {
                for (int j = 0; j < d.Rows[i].Cells.Count; j++)
                {
                    if (d[j, i].Value != null && d[j, i].Value.ToString() == content)
                    {
                        //返回单元格
                        return d[j, i];
                    }
                }
            }

            //返回null
            return null;
        }



        //根据结果设置单元格提示文本
        public static void SetCellToolTipText(DataGridView d, PingResult r2)
        {
            //遍历DataGridView
            for (int i = 0; i < d.Rows.Count; i++)
            {
                for (int j = 0; j < d.Rows[i].Cells.Count; j++)
                {
                    //判断内容=IP 
                    if (d[j, i].Value != null && d[j, i].Value.ToString() == r2.IP)
                    {
                        //判断结果
                        if (r2.State)
                        {
                            //true
                            d[j, i].ToolTipText = $"时延：{r2.RoundtripTime}ms";
                        }
                        else
                        {
                            //false
                            d[j, i].ToolTipText = "不可达";
                        }
                    }
                }
            }
        }

        //根据结果字典设置单元格提示文本
        public static void SetCellToolTipText(DataGridView d, Dictionary<string, PingResult> dic)
        {
            //遍历DataGridView
            for (int i = 0; i < d.Rows.Count; i++)
            {
                for (int j = 0; j < d.Rows[i].Cells.Count; j++)
                {
                    //判断字典包含该IP 
                    if (d[j, i].Value != null && dic.ContainsKey(d[j, i].Value.ToString()))
                    {
                        //获取行头
                        string rowTitle = d.Rows[i].HeaderCell.Value.ToString();
                        //获取列头
                        string columnTitle = d.Columns[j].HeaderText.ToString();
                        //从字典取出结果，判断结果
                        PingResult r2 = dic[d[j, i].Value.ToString()];
                        if (r2.State)
                        {
                            //true
                            d[j, i].ToolTipText = $"{columnTitle}-{rowTitle}-{r2.IP}{Environment.NewLine}时延：{r2.RoundtripTime}ms";
                        }
                        else
                        {
                            //false
                            d[j, i].ToolTipText = $"{columnTitle}-{rowTitle}-{r2.IP}{Environment.NewLine}不可达";
                        }
                    }
                }
            }
        }

        //根据结果字典设置单元格背景色
        public static void SetCellBackColor(DataGridView d, Dictionary<string, PingResult> dic)
        {
            //遍历DataGridView
            for (int i = 0; i < d.Rows.Count; i++)
            {
                for (int j = 0; j < d.Rows[i].Cells.Count; j++)
                {
                    //判断字典包含该IP 
                    if (d[j, i].Value != null && dic.ContainsKey(d[j, i].Value.ToString()))
                    {
                        //从字典取出结果，判断结果
                        PingResult r2 = dic[d[j, i].Value.ToString()];
                        if (r2.State)
                        {
                            //true
                            d[j, i].Style.BackColor = Tool.GetColorByDelay(r2.RoundtripTime);
                        }
                        else
                        {
                            //false，粉红色
                            d[j, i].Style.BackColor = Color.Pink;
                        }
                    }
                }
            }
        }

        //根据结果字典设置单元格背景色和提示文本
        public static void SetCellBackColorAndToolTipText(DataGridView d, Dictionary<string, PingResult> dic)
        {
            //遍历DataGridView
            for (int i = 0; i < d.Rows.Count; i++)
            {
                for (int j = 0; j < d.Rows[i].Cells.Count; j++)
                {
                    //判断字典包含该IP 
                    if (d[j, i].Value != null && dic.ContainsKey(d[j, i].Value.ToString()))
                    {
                        //获取行头
                        string rowTitle = d.Rows[i].HeaderCell.Value.ToString();
                        //获取列头
                        string columnTitle = d.Columns[j].HeaderText.ToString();
                        //从字典取出结果，判断结果
                        PingResult r2 = dic[d[j, i].Value.ToString()];
                        if (r2.State)
                        {
                            //true
                            d[j, i].Style.BackColor = Tool.GetColorByDelay(r2.RoundtripTime);
                            d[j, i].ToolTipText = $"{columnTitle}-{rowTitle}-{r2.IP}{Environment.NewLine}时延：{r2.RoundtripTime}ms";
                        }
                        else
                        {
                            //false，粉红色
                            d[j, i].Style.BackColor = Color.Pink;
                            d[j, i].ToolTipText = $"{columnTitle}-{rowTitle}-{r2.IP}{Environment.NewLine}不可达";
                        }
                    }
                }
            }
        }

    }
}
