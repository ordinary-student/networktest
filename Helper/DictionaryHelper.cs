﻿using System;
using System.Collections.Generic;


namespace NetworkTest
{
    //字典辅助类
    public class DictionaryHelper
    {
        //统计True结果数
        public static int CountTrueResult(Dictionary<string, PingResult> d)
        {
            //结果为true的数量
            int count = 0;
            if (d != null)
            {
                //遍历字典
                foreach (var item in d)
                {
                    //判断结果为true
                    if (item.Value.State)
                    {
                        //数量+1
                        count++;
                    }
                }
            }
            //返回数量
            return count;
        }
    }
}
