﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkTest
{
    //List辅助类
    public class ListHelper
    {
        //List转为字符串数组
        public static string[] ListToStringArray(List<string[]> list)
        {
            //
            List<string> rlist = new List<string>();
            //遍历
            for (int i = 0; i < list.Count; i++)
            {
                //遍历
                for (int j = 0; j < list[i].Length; j++)
                {
                    //添加
                    rlist.Add(list[i][j]);
                }
            }

            //返回
            return rlist.ToArray();
        }

        //List去重
        public static List<string[]> RemoveRepeat(List<string[]> list)
        {
            //
            string[] reference = new string[list.Count * list[0].Length];
            //去重后的字符串数组索引
            int k = 0;
            //遍历
            for (int i = 0; i < list.Count; i++)
            {
                //遍历
                for (int j = 0; j < list[i].Length; j++)
                {
                    //判断是否重复
                    if (Array.IndexOf(reference, list[i][j]) == -1)
                    {
                        //不重复
                        reference[k] = list[i][j];
                        k++;
                    }
                    else
                    {
                        //重复
                        list[i][j] = "";
                    }
                }
            }

            //
            return list;
        }
    }
}
