﻿using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace NetworkTest
{
    //进程辅助类
    public class ProcessHelper
    {
        //获取当前进程
        public static Process CurrentProcess = Process.GetCurrentProcess();
        //Cmd进程
        public static Process CmdProcess = GetCmdProcess();

        //获取cmd进程
        public static Process GetCmdProcess()
        {
            //新建cmd进程
            Process process = new Process();
            //调用程序
            process.StartInfo.FileName = "cmd.exe";
            //是否使用Shell
            process.StartInfo.UseShellExecute = false;
            //本程序接管输入输出流
            //重定向标准输入流
            process.StartInfo.RedirectStandardInput = true;
            //重定向标准输出流
            process.StartInfo.RedirectStandardOutput = true;
            //重定向标准错误流
            process.StartInfo.RedirectStandardError = true;
            //不显示DOS窗口
            process.StartInfo.CreateNoWindow = true;
            //运行cmd
            //process.Start();
            //返回
            return process;
        }

        //获取本程序的消耗内存
        public static string CurrentMemory()
        {
            //获取消耗内存
            long memoryUsed = CurrentProcess.WorkingSet64;
            //返回
            return ((int)(memoryUsed / 1048576)).ToString();
        }

        //Cmd执行方法
        public static void Cmd(string cmd)
        {
            //打开控制台输入
            CmdProcess.Start();
            //向cmd窗口发送输入信息
            CmdProcess.StandardInput.WriteLine(cmd);
            CmdProcess.StandardInput.AutoFlush = true;
            //关闭控制台输入
            CmdProcess.StandardInput.Close();
        }

        //Cmd执行方法带结果
        public static string Cmd2(string cmd)
        {
            //打开控制台输入
            CmdProcess.Start();
            //向cmd窗口发送输入信息
            CmdProcess.StandardInput.WriteLine(cmd);
            CmdProcess.StandardInput.AutoFlush = true;
            //关闭控制台输入
            CmdProcess.StandardInput.Close();
            //异步读取结果
            string outText = CmdProcess.StandardOutput.ReadToEndAsync().Result;
            string errText = CmdProcess.StandardError.ReadToEndAsync().Result;
            //读取结果
            //string outText = CmdProcess.StandardOutput.ReadToEnd();
            return $"{outText}{errText}";
        }

    }
}
