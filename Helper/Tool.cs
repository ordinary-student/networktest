﻿using System;
using System.Drawing;

namespace NetworkTest
{
    //工具类
    public class Tool
    {
        //当前时间
        public static string NowTime()
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        //根据时延返回不同颜色
        public static Color GetColorByDelay(long time)
        {
            //判断时延
            if (time < 50)
            {
                //绿色
                return Color.LimeGreen;
            }
            else if (time >= 50 && time <= 100)
            {
                //淡绿色
                return Color.LightGreen;
            }
            else
            {
                //淡黄色
                return Color.Beige;
            }
        }

        //字符串数组去除重复元素
        public static string[] RemoveRepeat(string[] oldS)
        {
            //去重后的字符串数组
            string[] newS = new string[oldS.Length];
            //去重后的字符串数组索引
            int k = 0;
            //遍历字符串数组
            for (int i = 0; i < oldS.Length; i++)
            {
                //判断是否重复
                if (Array.IndexOf(newS, oldS[i]) == -1)
                {
                    //不重复，添加到新数组
                    newS[k] = oldS[i];
                    k++;
                }
            }
            //返回新数组
            return newS;
        }

    }
}
