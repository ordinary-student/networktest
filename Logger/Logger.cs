﻿using System;
using System.Collections.Generic;


namespace NetworkTest
{
    //日志记录器类
    public class Logger
    {
        //日志队列
        private Queue<string> Queue = new Queue<string>(100);

        //构造方法
        public Logger()
        {
           
        }

        //记录日志
        public void Log(string log)
        {
            //日志消息
            //string message = $"[{Tool.NowTime()}] {log}";          
            //加入队列
            //Queue.Enqueue(message);
            Queue.Enqueue(log);
        }

        //取出日志
        public string GetLog()
        {
            //取出日志
            return Queue.Dequeue();
        }

        //判断日志队列不为空
        public bool HasLog()
        {
            return Queue.Count > 0;
        }
    }
}
