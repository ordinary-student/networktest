﻿using System;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace NetworkTest
{
    internal static class Program
    {
        //APP名称
        public const string APP_NAME = "NetworkTest";
        //APP描述
        public const string APP_INFO = "项目网络连接情况检测";
        //作者
        public const string AUTHOR = "CHControl";
        //链接
        public const string HREF = "http://www.chcontrol.com/cn/";
        //版本
        public static string APP_VERSION = GetVersion();
        //最后更新时间
        public const string LAST_UPDATE_TIME = "2023-04-17";
        //
        public static string APP_ABOUT = $"{APP_NAME}{Environment.NewLine}{APP_INFO}{Environment.NewLine}Version：{APP_VERSION}{Environment.NewLine}Author：{AUTHOR} {Environment.NewLine}Home：{HREF}{Environment.NewLine}LastUpdate：{LAST_UPDATE_TIME}";

        //模板名字
        public const string TEMPLATE_NAME = "设备IP地址模板.xlsx";

        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {          
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        //获取本程序版本
        public static string GetVersion()
        {
            //分割IP地址
            string[] vs = Assembly.GetExecutingAssembly().GetName().Version.ToString().Split(new char[] { '.' });
            //只取3位
            return $"{vs[0]}.{vs[1]}.{vs[2]}";
        }


    }
}
