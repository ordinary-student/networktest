﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace NetworkTest
{
    //Ping进程类
    public class PingProcess
    {
        //IP地址
        public string IP { get; set; }
        //Ping请求数
        public int Number { get; set; }
        //Ping进程
        private readonly Process Process;
        private readonly string DelayPattern = @"时间[<|=]\d+MS";

        //构造方法
        public PingProcess()
        {
            //创建进程对象
            Process = new Process();
            //调用程序
            Process.StartInfo.FileName = "ping.exe";
            //是否使用Shell
            Process.StartInfo.UseShellExecute = false;
            //本程序接管输入输出流
            //重定向标准输入流
            Process.StartInfo.RedirectStandardInput = true;
            //重定向标准输出流
            Process.StartInfo.RedirectStandardOutput = true;
            //重定向标准错误流
            Process.StartInfo.RedirectStandardError = true;
            //不显示DOS窗口
            Process.StartInfo.CreateNoWindow = true;
        }

        //构造方法
        public PingProcess(string ip, int number) : this()
        {
            IP = ip;
            Number = number;
            //设置参数
            Process.StartInfo.Arguments = $"{ip} -n {number}";
        }

        //构造方法
        public PingProcess(string host) : this(host, 4)
        {
        }

        //执行Ping
        public PingResult Execute()
        {
            try
            {
                //执行Ping
                Process.Start();
                //读取结果
                string outText = Process.StandardOutput.ReadToEnd();
                //Console.WriteLine(outText);
                string upperOutText = outText.ToUpper();
                //状态
                bool state = upperOutText.Contains("TTL");
                //计算平均时延
                int sum = 0;
                int avg = -1;
                //结果为TRUE
                if (state)
                {
                    //用正则表达式匹配
                    MatchCollection matches = Regex.Matches(upperOutText, DelayPattern);
                    //遍历打印
                    foreach (Match match in matches)
                    {
                        //截取字符串
                        string time = match.Value.Replace("时间<", "").Replace("时间=", "").Replace("MS", "");
                        //转为INT
                        int timeInt = int.Parse(time);
                        //Console.WriteLine(timeInt);
                        sum += timeInt;
                    }

                    if (matches.Count > 0)
                    {
                        //求时延平均值
                        avg = sum / matches.Count;
                    }
                }

                //返回
                return new PingResult(IP, state, avg);
            }
            catch (Exception)
            {
                return new PingResult(IP, false, -1);
            }
        }

        //执行Ping
        public PingResult Execute(string ip, int number)
        {
            IP = ip;
            Number = number;
            //设置参数
            Process.StartInfo.Arguments = $"{ip} -n {number}";

            //返回
            return Execute();
        }

    }
}
