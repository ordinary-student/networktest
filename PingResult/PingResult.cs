﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkTest
{
    //Ping结果类2
    public class PingResult
    {
        //ip
        public string IP { get; set; }
        //状态
        public bool State { get; set; }
        //时延
        public long RoundtripTime { get; set; }

        //构造方法
        public PingResult()
        {
        }

        //构造方法
        public PingResult(string ip, bool state, long time)
        {
            IP = ip;
            State = state;
            RoundtripTime = time;
        }

        //ToString方法
        public override string ToString()
        {
            return $"{IP}={State}={RoundtripTime}";
        }
    }
}
