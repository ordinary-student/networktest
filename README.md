# NetworkTest

#### 介绍
根据IP地址模板生成DataGridView，批量Ping，用于测试项目中各个设备的网络连接情况

#### 界面
![](https://gitee.com/ordinary-student/networktest/raw/master/test/1.png)
![](https://gitee.com/ordinary-student/networktest/raw/master/test/2.png)
