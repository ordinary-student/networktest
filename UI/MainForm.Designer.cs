﻿namespace NetworkTest
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.TabPage1 = new System.Windows.Forms.TabPage();
            this.NetworkGroupBox = new System.Windows.Forms.GroupBox();
            this.IPDataGridView = new System.Windows.Forms.DataGridView();
            this.ProjectGroupBox = new System.Windows.Forms.GroupBox();
            this.PingButton = new System.Windows.Forms.Button();
            this.SelectFileButton = new System.Windows.Forms.Button();
            this.FilePathTextBox = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.TabPage2 = new System.Windows.Forms.TabPage();
            this.MacGroupBox = new System.Windows.Forms.GroupBox();
            this.MacTextBox = new System.Windows.Forms.TextBox();
            this.GetMacButton = new System.Windows.Forms.Button();
            this.ToolGroupBox = new System.Windows.Forms.GroupBox();
            this.CreateTemplateButton = new System.Windows.Forms.Button();
            this.AboutButton = new System.Windows.Forms.Button();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LogToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.InternetToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.DelayToolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.DelayToolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.DelayToolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.DelayToolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.MousePosToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.MemoryToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.NowTimeToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.LogTimer = new System.Windows.Forms.Timer(this.components);
            this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.StatusTimer = new System.Windows.Forms.Timer(this.components);
            this.OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.InternetTimer = new System.Windows.Forms.Timer(this.components);
            this.TabControl1.SuspendLayout();
            this.TabPage1.SuspendLayout();
            this.NetworkGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IPDataGridView)).BeginInit();
            this.ProjectGroupBox.SuspendLayout();
            this.TabPage2.SuspendLayout();
            this.MacGroupBox.SuspendLayout();
            this.ToolGroupBox.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabControl1
            // 
            this.TabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.TabControl1.Controls.Add(this.TabPage1);
            this.TabControl1.Controls.Add(this.TabPage2);
            this.TabControl1.Location = new System.Drawing.Point(0, 5);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(884, 635);
            this.TabControl1.TabIndex = 0;
            // 
            // TabPage1
            // 
            this.TabPage1.Controls.Add(this.NetworkGroupBox);
            this.TabPage1.Controls.Add(this.ProjectGroupBox);
            this.TabPage1.Location = new System.Drawing.Point(4, 26);
            this.TabPage1.Name = "TabPage1";
            this.TabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage1.Size = new System.Drawing.Size(876, 605);
            this.TabPage1.TabIndex = 0;
            this.TabPage1.Text = "设备IP地址";
            this.TabPage1.UseVisualStyleBackColor = true;
            // 
            // NetworkGroupBox
            // 
            this.NetworkGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NetworkGroupBox.Controls.Add(this.IPDataGridView);
            this.NetworkGroupBox.Location = new System.Drawing.Point(5, 92);
            this.NetworkGroupBox.Name = "NetworkGroupBox";
            this.NetworkGroupBox.Size = new System.Drawing.Size(863, 507);
            this.NetworkGroupBox.TabIndex = 5;
            this.NetworkGroupBox.TabStop = false;
            this.NetworkGroupBox.Text = "网络检测";
            // 
            // IPDataGridView
            // 
            this.IPDataGridView.AllowUserToAddRows = false;
            this.IPDataGridView.AllowUserToDeleteRows = false;
            this.IPDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IPDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.IPDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.IPDataGridView.Location = new System.Drawing.Point(5, 23);
            this.IPDataGridView.MultiSelect = false;
            this.IPDataGridView.Name = "IPDataGridView";
            this.IPDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.IPDataGridView.RowTemplate.Height = 23;
            this.IPDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.IPDataGridView.Size = new System.Drawing.Size(848, 478);
            this.IPDataGridView.TabIndex = 6;
            // 
            // ProjectGroupBox
            // 
            this.ProjectGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ProjectGroupBox.Controls.Add(this.PingButton);
            this.ProjectGroupBox.Controls.Add(this.SelectFileButton);
            this.ProjectGroupBox.Controls.Add(this.FilePathTextBox);
            this.ProjectGroupBox.Controls.Add(this.Label1);
            this.ProjectGroupBox.Location = new System.Drawing.Point(5, 5);
            this.ProjectGroupBox.Name = "ProjectGroupBox";
            this.ProjectGroupBox.Size = new System.Drawing.Size(863, 79);
            this.ProjectGroupBox.TabIndex = 0;
            this.ProjectGroupBox.TabStop = false;
            this.ProjectGroupBox.Text = "项目名称";
            // 
            // PingButton
            // 
            this.PingButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PingButton.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.PingButton.ForeColor = System.Drawing.Color.Green;
            this.PingButton.Location = new System.Drawing.Point(777, 28);
            this.PingButton.Name = "PingButton";
            this.PingButton.Size = new System.Drawing.Size(80, 35);
            this.PingButton.TabIndex = 4;
            this.PingButton.Text = "Ping";
            this.PingButton.UseVisualStyleBackColor = true;
            this.PingButton.Click += new System.EventHandler(this.PingButton_Click);
            // 
            // SelectFileButton
            // 
            this.SelectFileButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SelectFileButton.Location = new System.Drawing.Point(692, 28);
            this.SelectFileButton.Name = "SelectFileButton";
            this.SelectFileButton.Size = new System.Drawing.Size(80, 35);
            this.SelectFileButton.TabIndex = 3;
            this.SelectFileButton.Text = "选择文件";
            this.SelectFileButton.UseVisualStyleBackColor = true;
            this.SelectFileButton.Click += new System.EventHandler(this.SelectFileButton_Click);
            // 
            // FilePathTextBox
            // 
            this.FilePathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FilePathTextBox.Location = new System.Drawing.Point(5, 40);
            this.FilePathTextBox.Name = "FilePathTextBox";
            this.FilePathTextBox.Size = new System.Drawing.Size(681, 23);
            this.FilePathTextBox.TabIndex = 2;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(5, 20);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(147, 14);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "设备IP地址模板文件：";
            // 
            // TabPage2
            // 
            this.TabPage2.Controls.Add(this.MacGroupBox);
            this.TabPage2.Controls.Add(this.ToolGroupBox);
            this.TabPage2.Location = new System.Drawing.Point(4, 26);
            this.TabPage2.Name = "TabPage2";
            this.TabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage2.Size = new System.Drawing.Size(876, 605);
            this.TabPage2.TabIndex = 1;
            this.TabPage2.Text = "工具";
            this.TabPage2.UseVisualStyleBackColor = true;
            // 
            // MacGroupBox
            // 
            this.MacGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MacGroupBox.Controls.Add(this.MacTextBox);
            this.MacGroupBox.Controls.Add(this.GetMacButton);
            this.MacGroupBox.Location = new System.Drawing.Point(5, 130);
            this.MacGroupBox.Name = "MacGroupBox";
            this.MacGroupBox.Size = new System.Drawing.Size(863, 469);
            this.MacGroupBox.TabIndex = 2;
            this.MacGroupBox.TabStop = false;
            this.MacGroupBox.Text = "获取物理地址";
            // 
            // MacTextBox
            // 
            this.MacTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MacTextBox.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MacTextBox.Location = new System.Drawing.Point(10, 91);
            this.MacTextBox.Multiline = true;
            this.MacTextBox.Name = "MacTextBox";
            this.MacTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.MacTextBox.Size = new System.Drawing.Size(842, 372);
            this.MacTextBox.TabIndex = 1;
            // 
            // GetMacButton
            // 
            this.GetMacButton.Location = new System.Drawing.Point(10, 32);
            this.GetMacButton.Name = "GetMacButton";
            this.GetMacButton.Size = new System.Drawing.Size(126, 42);
            this.GetMacButton.TabIndex = 0;
            this.GetMacButton.Text = "获取Mac地址表";
            this.GetMacButton.UseVisualStyleBackColor = true;
            this.GetMacButton.Click += new System.EventHandler(this.GetMacButton_Click);
            // 
            // ToolGroupBox
            // 
            this.ToolGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ToolGroupBox.Controls.Add(this.CreateTemplateButton);
            this.ToolGroupBox.Controls.Add(this.AboutButton);
            this.ToolGroupBox.Location = new System.Drawing.Point(5, 7);
            this.ToolGroupBox.Name = "ToolGroupBox";
            this.ToolGroupBox.Size = new System.Drawing.Size(863, 105);
            this.ToolGroupBox.TabIndex = 1;
            this.ToolGroupBox.TabStop = false;
            this.ToolGroupBox.Text = "创建模板";
            // 
            // CreateTemplateButton
            // 
            this.CreateTemplateButton.Location = new System.Drawing.Point(10, 32);
            this.CreateTemplateButton.Name = "CreateTemplateButton";
            this.CreateTemplateButton.Size = new System.Drawing.Size(213, 42);
            this.CreateTemplateButton.TabIndex = 0;
            this.CreateTemplateButton.Text = "创建设备IP地址Excel模板";
            this.CreateTemplateButton.UseVisualStyleBackColor = true;
            this.CreateTemplateButton.Click += new System.EventHandler(this.CreateTemplateButton_Click);
            // 
            // AboutButton
            // 
            this.AboutButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AboutButton.Location = new System.Drawing.Point(775, 32);
            this.AboutButton.Name = "AboutButton";
            this.AboutButton.Size = new System.Drawing.Size(73, 42);
            this.AboutButton.TabIndex = 1;
            this.AboutButton.Text = "关于";
            this.AboutButton.UseVisualStyleBackColor = true;
            this.AboutButton.Click += new System.EventHandler(this.AboutButton_Click);
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LogToolStripStatusLabel,
            this.InternetToolStripStatusLabel,
            this.DelayToolStripStatusLabel1,
            this.DelayToolStripStatusLabel2,
            this.DelayToolStripStatusLabel3,
            this.DelayToolStripStatusLabel4,
            this.MousePosToolStripStatusLabel,
            this.MemoryToolStripStatusLabel,
            this.NowTimeToolStripStatusLabel});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 639);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.ShowItemToolTips = true;
            this.StatusStrip1.Size = new System.Drawing.Size(884, 22);
            this.StatusStrip1.TabIndex = 1;
            this.StatusStrip1.Text = "状态栏";
            // 
            // LogToolStripStatusLabel
            // 
            this.LogToolStripStatusLabel.AutoToolTip = true;
            this.LogToolStripStatusLabel.DoubleClickEnabled = true;
            this.LogToolStripStatusLabel.ForeColor = System.Drawing.Color.Blue;
            this.LogToolStripStatusLabel.Name = "LogToolStripStatusLabel";
            this.LogToolStripStatusLabel.Size = new System.Drawing.Size(241, 17);
            this.LogToolStripStatusLabel.Spring = true;
            this.LogToolStripStatusLabel.Text = "就绪";
            this.LogToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LogToolStripStatusLabel.DoubleClick += new System.EventHandler(this.LogToolStripStatusLabel_DoubleClick);
            // 
            // InternetToolStripStatusLabel
            // 
            this.InternetToolStripStatusLabel.AutoToolTip = true;
            this.InternetToolStripStatusLabel.Name = "InternetToolStripStatusLabel";
            this.InternetToolStripStatusLabel.Size = new System.Drawing.Size(92, 17);
            this.InternetToolStripStatusLabel.Text = "无法连接互联网";
            // 
            // DelayToolStripStatusLabel1
            // 
            this.DelayToolStripStatusLabel1.BackColor = System.Drawing.Color.LimeGreen;
            this.DelayToolStripStatusLabel1.Name = "DelayToolStripStatusLabel1";
            this.DelayToolStripStatusLabel1.Size = new System.Drawing.Size(72, 17);
            this.DelayToolStripStatusLabel1.Text = "时延<50ms";
            // 
            // DelayToolStripStatusLabel2
            // 
            this.DelayToolStripStatusLabel2.BackColor = System.Drawing.Color.LightGreen;
            this.DelayToolStripStatusLabel2.Name = "DelayToolStripStatusLabel2";
            this.DelayToolStripStatusLabel2.Size = new System.Drawing.Size(69, 17);
            this.DelayToolStripStatusLabel2.Text = "50~100ms";
            // 
            // DelayToolStripStatusLabel3
            // 
            this.DelayToolStripStatusLabel3.BackColor = System.Drawing.Color.Beige;
            this.DelayToolStripStatusLabel3.Name = "DelayToolStripStatusLabel3";
            this.DelayToolStripStatusLabel3.Size = new System.Drawing.Size(55, 17);
            this.DelayToolStripStatusLabel3.Text = ">100ms";
            // 
            // DelayToolStripStatusLabel4
            // 
            this.DelayToolStripStatusLabel4.BackColor = System.Drawing.Color.Pink;
            this.DelayToolStripStatusLabel4.Name = "DelayToolStripStatusLabel4";
            this.DelayToolStripStatusLabel4.Size = new System.Drawing.Size(36, 17);
            this.DelayToolStripStatusLabel4.Text = "∞ms";
            // 
            // MousePosToolStripStatusLabel
            // 
            this.MousePosToolStripStatusLabel.AutoToolTip = true;
            this.MousePosToolStripStatusLabel.BackColor = System.Drawing.Color.LightCyan;
            this.MousePosToolStripStatusLabel.Name = "MousePosToolStripStatusLabel";
            this.MousePosToolStripStatusLabel.Size = new System.Drawing.Size(88, 17);
            this.MousePosToolStripStatusLabel.Text = "X:0000,Y:0000";
            // 
            // MemoryToolStripStatusLabel
            // 
            this.MemoryToolStripStatusLabel.AutoToolTip = true;
            this.MemoryToolStripStatusLabel.BackColor = System.Drawing.Color.LightSteelBlue;
            this.MemoryToolStripStatusLabel.Name = "MemoryToolStripStatusLabel";
            this.MemoryToolStripStatusLabel.Size = new System.Drawing.Size(90, 17);
            this.MemoryToolStripStatusLabel.Text = "消耗内存00MB";
            // 
            // NowTimeToolStripStatusLabel
            // 
            this.NowTimeToolStripStatusLabel.AutoToolTip = true;
            this.NowTimeToolStripStatusLabel.BackColor = System.Drawing.Color.PaleTurquoise;
            this.NowTimeToolStripStatusLabel.Name = "NowTimeToolStripStatusLabel";
            this.NowTimeToolStripStatusLabel.Size = new System.Drawing.Size(126, 17);
            this.NowTimeToolStripStatusLabel.Text = "0000-00-00 00:00:00";
            // 
            // LogTimer
            // 
            this.LogTimer.Interval = 1000;
            this.LogTimer.Tick += new System.EventHandler(this.LogTimer_Tick);
            // 
            // StatusTimer
            // 
            this.StatusTimer.Tick += new System.EventHandler(this.StatusTimer_Tick);
            // 
            // OpenFileDialog
            // 
            this.OpenFileDialog.Filter = "Excel文件|*.xlsx";
            this.OpenFileDialog.RestoreDirectory = true;
            this.OpenFileDialog.Title = "选择设备IP地址模板文件";
            this.OpenFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.OpenFileDialog_FileOk);
            // 
            // InternetTimer
            // 
            this.InternetTimer.Interval = 3000;
            this.InternetTimer.Tick += new System.EventHandler(this.InternetTimer_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 661);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.TabControl1);
            this.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "MainForm";
            this.Text = "项目名称-网络连接情况检测";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.TabControl1.ResumeLayout(false);
            this.TabPage1.ResumeLayout(false);
            this.NetworkGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.IPDataGridView)).EndInit();
            this.ProjectGroupBox.ResumeLayout(false);
            this.ProjectGroupBox.PerformLayout();
            this.TabPage2.ResumeLayout(false);
            this.MacGroupBox.ResumeLayout(false);
            this.MacGroupBox.PerformLayout();
            this.ToolGroupBox.ResumeLayout(false);
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl TabControl1;
        private System.Windows.Forms.TabPage TabPage1;
        private System.Windows.Forms.TabPage TabPage2;
        private System.Windows.Forms.Button CreateTemplateButton;
        private System.Windows.Forms.GroupBox ToolGroupBox;
        private System.Windows.Forms.StatusStrip StatusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel LogToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel InternetToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel DelayToolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel DelayToolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel DelayToolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel DelayToolStripStatusLabel4;
        private System.Windows.Forms.ToolStripStatusLabel MousePosToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel MemoryToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel NowTimeToolStripStatusLabel;
        private System.Windows.Forms.Timer LogTimer;
        private System.Windows.Forms.ToolTip ToolTip;
        private System.Windows.Forms.GroupBox NetworkGroupBox;
        private System.Windows.Forms.GroupBox ProjectGroupBox;
        private System.Windows.Forms.Button PingButton;
        private System.Windows.Forms.Label Label1;
        private System.Windows.Forms.DataGridView IPDataGridView;
        private System.Windows.Forms.TextBox FilePathTextBox;
        private System.Windows.Forms.Button SelectFileButton;
        private System.Windows.Forms.Button AboutButton;
        private System.Windows.Forms.Timer StatusTimer;
        private System.Windows.Forms.OpenFileDialog OpenFileDialog;
        private System.Windows.Forms.Timer InternetTimer;
        private System.Windows.Forms.GroupBox MacGroupBox;
        private System.Windows.Forms.Button GetMacButton;
        private System.Windows.Forms.TextBox MacTextBox;
    }
}

