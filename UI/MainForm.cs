﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Windows.Forms;
using System.Net.NetworkInformation;
using System.Threading.Tasks;


namespace NetworkTest
{  
    //主界面类
    public partial class MainForm : Form
    {     
        //日志记录器
        public Logger Logger = new Logger();
        //模板路径
        public string TemplateFilePath = $"{Application.StartupPath}\\{Program.TEMPLATE_NAME}";
        //
        public string OpenDir = Application.StartupPath;
        //测试互联网主机
        private readonly string InternetHost = "baidu.com";

        //定义一个键值对集合
        public Dictionary<string, PingResult> PingResultDictionary { get; set; }   
        //请求数和超时时间
        public static int PingRequestNumber = 1;
        public static int PingTimeOut = 500;

        //构造方法
        public MainForm()
        {
            //初始化组件
            InitializeComponent();
            MacTextBox.Text = IPHelper.GetMacList();
            PingResultDictionary = new Dictionary<string, PingResult>();      

            //判断模板文件是否存在
            if (File.Exists(TemplateFilePath))
            {
                //加载模板文件
                LoadTemplateFile(TemplateFilePath);
                FilePathTextBox.Text = TemplateFilePath;
            }
            else
            {
                //提示模板文件不存在
                Logger.Log($"模板文件不存在！请将模板文件放在程序目录下！模板文件名：{Program.TEMPLATE_NAME}");
            }
        }

        //加载模板文件
        private void LoadTemplateFile(string path)
        {
            try
            {
                //读取项目名称
                string projectName = ExcelHelper.ReadProjectName(path);
                Text = $"{projectName}-网络连接情况检测-本机IP：{IPHelper.LocalIPs}";
                ProjectGroupBox.Text = $"项目名称：{projectName}";
                //读取Excel内容写入DataGridView
                List<string[]> list = ExcelHelper.GetContent(path);
                //去重
                list = ListHelper.RemoveRepeat(list);
                //创建结果键值对
                PingResultDictionary = new Dictionary<string, PingResult>();
                //设置表格列头
                SetDataGridViewColumnHeader(list[0]);

                //遍历
                for (int i = 1; i < list.Count; i++)
                {
                    //获取一行数据
                    string[] row = list[i];
                    //新增一行
                    int index = IPDataGridView.Rows.Add();
                    //设置行头
                    IPDataGridView.Rows[index].HeaderCell.Value = row[0];
                    //遍历
                    for (int j = 1; j < row.Length; j++)
                    {
                        //添加单元格-列坐标，行坐标
                        IPDataGridView[j - 1, index].Value = row[j];
                        //获取列头
                        string columnTitle = IPDataGridView.Columns[j - 1].HeaderText.ToString();
                        //设置提示文本
                        IPDataGridView[j - 1, index].ToolTipText = $"{columnTitle}-{row[0]}-{row[j]}";
                    }
                }
            }
            catch
            {
                //提示
                Logger.Log($"加载模板文件失败!");
                return;
            }

            //提示
            Logger.Log($"模板文件读取成功!");
        }

        //设置表格列头
        private void SetDataGridViewColumnHeader(string[] arr)
        {
            //遍历数组
            for (int i = 1; i < arr.Length; i++)
            {
                //创建列
                DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn();
                //调整列宽
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                //禁用排序
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
                //设置列标题
                column.HeaderText = arr[i];
                //添加列             
                IPDataGridView.Columns.Add(column);
            }

            //左上角
            IPDataGridView.TopLeftHeaderCell.Value = arr[0];
        }

        //创建模板按钮单击事件
        private void CreateTemplateButton_Click(object sender, EventArgs e)
        {
            //创建模板
            string path = ExcelHelper.CreateExcelTemplate();
            OpenDir = path;
            if (FilePathTextBox.Text.Length <= 0)
            {
                TemplateFilePath = path;
                FilePathTextBox.Text = path;
            }
            Logger.Log($"创建模板成功！位置：{path}");
        }

        //刷新日志定时器触发事件
        private void LogTimer_Tick(object sender, EventArgs e)
        {
            //有日志
            if (Logger.HasLog())
            {
                LogToolStripStatusLabel.Text = Logger.GetLog();
                //提示文本
                LogToolStripStatusLabel.ToolTipText = LogToolStripStatusLabel.Text;
            }
        }

        //关于按钮单击事件
        private void AboutButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Program.APP_ABOUT, "关于");
        }

        //窗口首次显示事件
        private void MainForm_Shown(object sender, EventArgs e)
        {          
            //启动刷新日志定时器
            LogTimer.Start();        
            StatusTimer.Start();
            InternetTimer.Start();
        }

        //状态栏双击事件
        private void LogToolStripStatusLabel_DoubleClick(object sender, EventArgs e)
        {
            //是文件
            if (File.Exists(OpenDir))
            {
                //打开文件夹，选中文件
                Process.Start("explorer.exe", $"/select,{OpenDir}");
            }
            else if (Directory.Exists(OpenDir))
            {
                //打开文件夹
                Process.Start("explorer.exe", OpenDir);
            }
        }

        //Ping按钮单击事件
        private async void PingButton_Click(object sender, EventArgs e)
        {
            //设备数量
            int deviceCount = DataGridViewHelper.CountDevices(IPDataGridView);
            //如果设备数量大于0
            if (deviceCount > 0)
            {
                //禁用按钮
                PingButton.Enabled = false;
                IPDataGridView[0, 0].Selected = false;
                Logger.Log("正在检测中...");

                //创建结果键值对
                PingResultDictionary = new Dictionary<string, PingResult>(deviceCount);
                //初始化所有单元格背景白色
                DataGridViewHelper.InitCellsBackground(IPDataGridView, Color.White);
                //获取待检测的IP数组
                string[] ips = DataGridViewHelper.GetIPs(IPDataGridView);

                //创建任务数组
                Task<PingResult>[] tasks = new Task<PingResult>[ips.Length];
                //遍历IP数组
                for (int i = 0; i < ips.Length; i++)
                {
                    //创建任务
                    tasks[i] = PingAsync(ips[i]);
                }

                //总数
                int sum = ips.Length;
                //结果为True数量
                int count = 0;

                //循环等待所有任务完成
                while (tasks.Length > 0)
                {
                    //等待任意一个任务完成
                    Task<PingResult> completedTask = await Task.WhenAny(tasks);
                    //从任务数组中移除已完成的任务
                    tasks = tasks.Where(t => t != completedTask).ToArray();
                    //获取任务结果
                    PingResult pingResult = completedTask.Result;
                    //Console.WriteLine($"{TaskDictionary[completedTask]}: {pingResult.Status}: {pingResult.RoundtripTime}");

                    //获取IP
                    string ip = pingResult.IP;
                    //设置标签背景色
                    SetBackColor(pingResult);
                    //加入字典
                    PingResultDictionary.Add(ip, pingResult);
                    //结果为True数量+1
                    if (pingResult.State)
                    {
                        count++;
                    }

                    //显示结果数量
                    NetworkGroupBox.Text = $"网络检测-[ 设备数量：{sum} ] [ True：{count} ] [ False：{sum - count} ]";
                }

                //设置单元格提示文本
                DataGridViewHelper.SetCellToolTipText(IPDataGridView, PingResultDictionary);

                //启用按钮
                PingButton.Enabled = true;
                Logger.Log("检测完成！");            
            }

        }

        //执行异步Ping
        private async Task<PingResult> PingAsync(string ipAddress)
        {
            using (Ping ping = new Ping())
            {
                try
                {
                    //发送数据包次数
                    int count = PingRequestNumber;
                    //状态
                    bool state = false;
                    //时延
                    long roundtripTime = 0;
                    for (int i = 0; i < count; i++)
                    {
                        //连续发送N次Ping
                        PingReply reply = await ping.SendPingAsync(ipAddress, PingTimeOut);
                        //累加时延
                        roundtripTime = roundtripTime + reply.RoundtripTime;
                        //状态
                        state = reply.Status == IPStatus.Success;
                    }

                    //返回Ping结果
                    return new PingResult(ipAddress, state, roundtripTime / count);
                }
                catch (PingException e)
                {
                    Console.WriteLine($"Ping failed: {e.Message}");
                    //返回Ping结果
                    return new PingResult(ipAddress, false, 0);
                }
            }
        }
         
        //设置单元格背景色
        private void SetBackColor(PingResult r2)
        {
            DataGridViewHelper.SetCellBackColor(IPDataGridView, r2);
        }

        //选择文件按钮单击事件
        private void SelectFileButton_Click(object sender, EventArgs e)
        {
            Logger.Log($"选择模板文件中...");
            //路径文件存在
            if (File.Exists(FilePathTextBox.Text))
            {
                //设置对话框打开时的初始目录
                OpenFileDialog.InitialDirectory = Path.GetDirectoryName(FilePathTextBox.Text);
            }

            //打开选择文件对话框
            OpenFileDialog.ShowDialog();
        }

        //文件选择对话框OK事件
        private void OpenFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            if (OpenFileDialog.FileName.Length > 0)
            {
                //清空表-包括表头
                IPDataGridView.Columns.Clear();
                TemplateFilePath = OpenFileDialog.FileName;
                //加载模板文件
                LoadTemplateFile(TemplateFilePath);
                FilePathTextBox.Text = TemplateFilePath;
            }
        }
    
        //状态定时器触发事件
        private void StatusTimer_Tick(object sender, EventArgs e)
        {
            //设置状态栏                    
            MousePosToolStripStatusLabel.Text = $"X:{MousePosition.X},Y:{MousePosition.Y}";
            MemoryToolStripStatusLabel.Text = $"消耗内存{ProcessHelper.CurrentMemory()}MB";
            NowTimeToolStripStatusLabel.Text = $"{Tool.NowTime()}";
        }
      
        //互联网定时器触发事件
        private void InternetTimer_Tick(object sender, EventArgs e)
        {
            //创建Ping任务
            using (Ping ping = new Ping())
            {
                PingReply reply = ping.Send(InternetHost);
                //
                if (reply.Status == IPStatus.Success)
                {
                    InternetToolStripStatusLabel.Text = $"可以连接互联网-时延：{reply.RoundtripTime}ms";
                    InternetToolStripStatusLabel.ForeColor = Color.Green;
                }
                else
                {
                    InternetToolStripStatusLabel.Text = "无法连接互联网";
                    InternetToolStripStatusLabel.ForeColor = Color.Red;
                }
            }
        }

        //获取MAC地址按钮单击事件
        private void GetMacButton_Click(object sender, EventArgs e)
        {
            MacTextBox.Text = IPHelper.GetMacList();
            Logger.Log("获取Mac地址表成功!");
        }

        //主窗口关闭事件
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //停止定时器
            LogTimer.Stop();       
            StatusTimer.Stop();      
            InternetTimer.Stop();
        }
    }
}
